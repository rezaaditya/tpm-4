<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'name' => 'Yosef',
            'email' => 'yosef@gmail.com',
            'password' => bcrypt('yosef')
        ]);
        App\User::create([
            'name' => 'Rais',
            'email' => 'rais@gmail.com',
            'password' => bcrypt('rais')
        ]);
        App\Role::create([
            'name' => 'mahasiswa'
        ]);
        App\Role::create([
            'name' => 'dosen'
        ]);
        App\Role::create([
            'name' => 'aslab'
        ]);
        // $this->call(UsersTableSeeder::class);
    }
}
