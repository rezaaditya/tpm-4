<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/user', 'UserController@index');
Route::post('/user/add-role', 'UserController@addRole');
Route::get('/post', 'PostController@index');
Route::post('/post', 'PostController@store');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
