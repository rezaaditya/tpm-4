<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div style="border: 10px pink dashed">
        <form action="{{url('post')}}" method="post">
            @csrf
            <label for="">Title</label><input type="text" name="title"><br>
            <label for="content">Content</label><input type="text" name="content"><br>
            <label for="user_id">User</label>
            <select name="user_id" id="">
            @foreach($users as $user)
                <option value="{{$user->id}}">{{ $user->name }}</option>
            @endforeach
            </select><br>
            <input type="submit" value="Submit">
        </form>
    </div>
    <table border=1>
        <thead>
            <tr>
                <th>Title</th>
                <th>Author</th>
                <th>Content</th>
            </tr>
        </thead>
        <tbody>
            @foreach($posts as $post)
            <tr>
                <td>{{$post->title}}</td>
                <td>{{$post->user->name}}</td>
                <td>{{$post->content}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>

</body>
</html>