<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div style="border: 20px green dotted">
    <h2>Add new Role</h2>
    <form action="{{url('user/add-role')}}" method="post">
    @csrf
        <label for="user_id">User</label>
        <select name="user_id" id="">
            @foreach($users as $user)
            <option value="{{$user->id}}">{{ $user->name }}</option>
            @endforeach
        </select><br>
        <label for="role_id">Roles</label>
        <select name="role_id" id="">
            @foreach($roles as $role)
            <option value="{{$role->id}}">{{ $role->name }}</option>
            @endforeach
        </select><br>
        <input type="submit" value="Submit">
    </form>
    </div>
    @foreach($users as $user)
    <h2>{{$user->name}}</h2>
    <h3>Email: {{$user->email}}</h3>
    <p>Roles</p>
    <ul>
        @foreach($user->roles as $role)
        <li>{{$role->name}}</li>
        @endforeach
    </ul>
    <p>Posts</p>
    <div>
        @foreach($user->posts as $post)
        <h2>{{$post->title}}</h2>
        <p>Author: {{$post->user->name}}</p>
        <p>{{$post->content}}</p>
        @endforeach
    </div>
    <table border=1>
        <thead>
            <tr>
                <th>Title</th>
                <th>Author</th>
                <th>Content</th>
            </tr>
        </thead>
        <tbody>
            @foreach($user->posts as $post)
            <tr>
                <td>{{$post->title}}</td>
                <td>{{$post->user->name}}</td>
                <td>{{$post->content}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <hr>
    @endforeach
</body>
</html>