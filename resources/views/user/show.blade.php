<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h2>{{$user->name}}</h2>
    <h3>Email: {{$user->email}}</h3>
    <p>Roles</p>
    <ul>
        @foreach($user->roles as $role)
        <li>{{$role->name}}</li>
        @endforeach
    </ul>
    <p>Posts</p>
    <div>
        @foreach($user->posts as $post)
        <h2>{{$post->title}}</h2>
        <p>Author: {{$post->user->name}}</p>
        <p>{{$post->content}}</p>
        @endforeach
    </div>
    <table border=1>
        <thead>
            <tr>
                <th>Title</th>
                <th>Author</th>
                <th>Content</th>
            </tr>
        </thead>
        <tbody>
            @foreach($user->posts as $post)
            <tr>
                <td>{{$post->title}}</td>
                <td>{{$post->user->name}}</td>
                <td>{{$post->content}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>